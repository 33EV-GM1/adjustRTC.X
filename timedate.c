/*! \file  timedate.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date September 23, 2015, 4:47 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "timedate.h"

/*  isleap( year )
 *
 *  Determine if current year is a leap year.  Return TRUE or FALSE.
 *
 *  Argument:   year =  integer year.
 */

int isleap( int year )
{
return ( (year % 4 == 0 && year % 100 != 0) || (year % 400 == 0) );
}

/*  isletter
 *
 *  Return TRUE if character is alphabetic or underline
 */
int isletter(char c)
{
  return ((c>='a' && c<='z') || (c>='A' && c<='Z') || c=='_');
}

/*  _dayr( month, day, year )
 *
 *  Return the day of the year given month, day, year
 *
 *  Arguments:  month = integer month 1=jan, etc.
 *              day   = integer day of month 0..31
 *              year  = integer year 1980..
 *
 *  Returns:    Integer day of year 1..366
 */

int _dayr( mo, da, yr )
int mo, da, yr;
{
int i,day,leap;
static int day_tab[2][13] =
        { {0,31,28,31,30,31,30,31,31,30,31,30,31},
          {0,31,29,31,30,31,30,31,31,30,31,30,31} };
day = da;
leap = isleap( yr );
for (i=1;i<mo;i++) day += day_tab[leap][i];
return day;
}

/*  daywk       - Perpetual Calendar: Day of Week
 *
 *  Given a year from 1800..2099, a month 1..12 (Jan..Dec), and
 *  a day_of_month 1..31, determine the day of the week 0..6
 *  (0=Sunday, 1=Monday, etc.).
 *
 *  The perpetual calendar shown in the Smithsonian Tables is used; the
 *  Gregorian method employed, and tables are provided for the 19th, 20th
 *  and 21st centuries.
 *
 *  Requires "isleap" to determine if year is leap year.
 *
 *  int daywk( year, month, day )
 *  int year;
 *  int month;
 *  int day;
 *
 *  Returns:    Day of Week 0..6 (0=Sun, 1=Mon,...)
 */
/*  structure contains dominican letters A..G (0..6) for ordinary and
 *  leap years in each of three centuries.  To add centuries, refer to
 *  tables in Smithsonian Tables - Perpetual Calendar.
 */
struct PERPCAL {
 int ord18[29];
 int lep18[29];
 int ord19[29];
 int lep19[29];
 int ord20[29];
 int lep20[29];
 };

/*  structure to contain 2-dimensional array relating doninican letter 0..6 for
 *  each month to day of week for first day of each month:
 *
 *  x2[month][letter] = day_of_week
 */
struct PERPDAY {
 int x2[12][7];
 };

struct PERPCAL _perpcal =
{ {4,3,2,1,0,5,4,3,2,0,6,5,4,2,1,0,6,4,3,2,1,6,5,4,3,1,0,6,5},
  {4,3,2,1,6,5,4,3,1,0,6,5,3,2,1,0,5,4,3,2,0,6,5,4,2,1,0,6,4},
  {6,5,4,3,2,0,6,5,4,2,1,0,6,4,3,2,1,6,5,4,3,1,0,6,5,3,2,1,0},
  {6,5,4,3,1,0,6,5,3,2,1,0,5,4,3,2,0,6,5,4,2,1,0,6,4,3,2,1,6},
  {1,6,5,4,3,1,0,6,5,3,2,1,0,5,4,3,2,0,6,5,4,2,1,0,6,4,3,2,1},
  {0,6,5,4,2,1,0,6,4,3,2,1,6,5,4,3,1,0,6,5,3,2,1,0,5,4,3,2,0} };


struct PERPDAY _perpday =
{ 0,6,5,4,3,2,1,  3,2,1,0,6,5,4,  3,2,1,0,6,5,4,
  6,5,4,3,2,1,0,  1,0,6,5,4,3,2,  4,3,2,1,0,6,5,  6,5,4,3,2,1,0,
  2,1,0,6,5,4,3,  5,4,3,2,1,0,6,  0,6,5,4,3,2,1,  3,2,1,0,6,5,4,
  5,4,3,2,1,0,6 };


int daywk( int year, int month, int day )
{

int century, yr, iyear, letter, day1, day_of_week;
int leap;

century = (year-1800)/100;      /* 0=1800, etc  */
iyear = year % 100;             /* 0..99        */
leap = isleap(year);            /* TRUE if leap year    */
if (iyear<=28) yr=iyear;                        /* 0..28        */
else if ((iyear>28)&&(iyear<57)) yr=iyear-28;   /* 29..56       */
else if ((iyear>56)&&(iyear<85)) yr=iyear-56;   /* 57..84       */
else if ((iyear>84))             yr=iyear-84;   /* 85..99       */

/*  access proper column, get letter for the year               */
if      ((century==0)&&!leap)   letter = _perpcal.ord18[yr];
else if ((century==0)&&leap)    letter = _perpcal.lep18[yr];
else if ((century==1)&&!leap)   letter = _perpcal.ord19[yr];
else if ((century==1)&&leap)    letter = _perpcal.lep19[yr];
else if ((century==2)&&!leap)   letter = _perpcal.ord20[yr];
else if ((century==2)&&leap)    letter = _perpcal.lep20[yr];

/* now determine first day of week (0..6) from month, letter    */
day1 = _perpday.x2[(month-1)][letter];

/* given day of month 1..31, determine day of week using "day1" */
day_of_week = ((day-1) + day1) % 7;

return day_of_week;
}
