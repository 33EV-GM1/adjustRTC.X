/*! \file  timedate.h
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date September 23, 2015, 4:47 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */

#ifndef TIMEDATE_H
#define	TIMEDATE_H

#ifdef	__cplusplus
extern "C"
{
#endif

int _dayr( int, int, int );
int isleap( int );
int isletter( char );
int daywk( int, int, int );




#ifdef	__cplusplus
}
#endif

#endif	/* TIMEDATE_H */

