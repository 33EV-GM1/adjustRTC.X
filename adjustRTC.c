/*! \file  adjustRTC.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date September 21, 2015, 8:33 AM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>
#include <stdio.h>
#include "../include/dsPIC-EL-GM.h"
#include "../include/LCD.h"
#include "i2c.h"
#include "timedate.h"

// Fast RC oscillator with PLL
#pragma config FNOSC = FRCPLL
// Watchdog timer off
#pragma config FWDTEN = OFF
// Deadman timer off
#pragma config DMTEN = DISABLE

#define DS1307_ADDRESS 0x68

unsigned char DS1307readByte( unsigned char, unsigned int);
void DS1307writeByte( unsigned char, unsigned int, unsigned char);

int nLevel;
int nPrevPos[4];

char szLevels[6][12]={
  "Seconds ","Minutes ","Hours   ","Days    ","Months  ","Years   "
};

int toDecimal( unsigned char nHexValue )
{
  int result;

  result = (nHexValue&0x0f) + ((nHexValue&0xf0)>>4) * 10;
  return result;
}

unsigned char toHex( int nDecimal )
{
  return ( ((nDecimal/10)<<4) + (nDecimal%10) );
}


void showDecimal(int nValue,int nPosition)
{
  char szWork[16];
  sprintf(szWork,"%02d      ",nValue);
  LCDposition(nPosition);
  LCDputs(szWork);
}

int checkButtons( int *nValue )
{
  int changed;

  changed = 0;
  if ( BUTTON_LEFT )
    {
      changed = 1;
      *nValue = (*nValue) - 1;
    }
  if ( BUTTON_RIGHT )
    {
      changed = 1;
      *nValue = (*nValue) + 1;
    }
  return changed;
}

void adjustDayOfWeek( void )
{
  int mo,da,yr;
  int dow;

   da = toDecimal(DS1307readByte(DS1307_ADDRESS, (unsigned int) 4));
   mo = toDecimal(DS1307readByte(DS1307_ADDRESS, (unsigned int) 5));
   yr = toDecimal(DS1307readByte(DS1307_ADDRESS, (unsigned int) 6));
   yr = yr + 2000;
   dow = daywk( yr, mo, da );
   DS1307writeByte(DS1307_ADDRESS,
                   (unsigned int)3,
                   dow);
   Delay_ms(10); // AT24C16 has 5 ms write cycle time
}

/*! main - */

/*!
 *
 */
int main(void)
{
  char szWork[16];
  unsigned char hexValue;
  int decimalValue;

  Delay_ms(500);
  OLEDinit();
  InitI2C();
  LCDclear();
  Delay_ms(500);
  LCDputs("   Adjust RTC   ");
  Delay_ms(1000);

  /* SW1 (RB5) has no ANSEL bit */
  ANSELBbits.ANSB8 = 0;
  ANSELBbits.ANSB9 = 0;
  ANSELAbits.ANSA4 = 0;

  /* Buttons to inputs (redundant) */
  _TRISB5 = 1;
  _TRISB9 = 1;
  _TRISB8 = 1;
  _TRISA4 = 1;
  nPrevPos[0]=nPrevPos[1]=nPrevPos[2]=nPrevPos[3]=1;

  nLevel = 0;
  LCDposition(0x40);
  LCDputs(szLevels[nLevel]);

  while(1)
    {
//      sprintf(szWork,"%d %d %d %d         ",
//              BUTTON_UP,BUTTON_DOWN,BUTTON_LEFT,BUTTON_RIGHT);
//      LCDhome();
      LCDputs(szWork);
      if ( !BUTTON_DOWN )
        {
          if ( nPrevPos[0] )
            {
              nLevel++;
              if ( nLevel>5 )
                nLevel = 0;
              LCDposition(0x40);
              LCDputs(szLevels[nLevel]);
            }
        }
      nPrevPos[0] = BUTTON_DOWN;
      nPrevPos[1] = BUTTON_LEFT;
      nPrevPos[2] = BUTTON_RIGHT;
      switch ( nLevel )
        {
          case 0:   // Seconds
            hexValue = DS1307readByte(DS1307_ADDRESS, (unsigned int) 0);
            decimalValue = toDecimal(hexValue);
            showDecimal(decimalValue,0x48);
            if (checkButtons(&decimalValue) )
              {
                if ( decimalValue > 59 )
                  decimalValue = 0;
                if ( decimalValue < 0 )
                  decimalValue = 59;
                DS1307writeByte(DS1307_ADDRESS,
                        (unsigned int)0,
                        toHex(decimalValue));
                Delay_ms(10); // AT24C16 has 5 ms write cycle time
                showDecimal(decimalValue,0x48);
                Delay_ms(500);
              }
            break;
          case 1:   // Minutes
            hexValue = DS1307readByte(DS1307_ADDRESS, (unsigned int) 1);
            decimalValue = toDecimal(hexValue);
            showDecimal(decimalValue,0x48);
            if (checkButtons(&decimalValue) )
              {
                DS1307writeByte(DS1307_ADDRESS,
                        (unsigned int)1,
                        toHex(decimalValue));
                Delay_ms(10); // AT24C16 has 5 ms write cycle time
                showDecimal(decimalValue,0x48);
                Delay_ms(500);
              }
            break;
          case 2:   // Hours
            hexValue = DS1307readByte(DS1307_ADDRESS, (unsigned int) 2);
            hexValue &= 0x3f;
            decimalValue = toDecimal(hexValue);
            showDecimal(decimalValue,0x48);
            if (checkButtons(&decimalValue) )
              {
                DS1307writeByte(DS1307_ADDRESS,
                        (unsigned int)2,
                        (toHex(decimalValue)&0x3f));
                Delay_ms(10); // AT24C16 has 5 ms write cycle time
                showDecimal(decimalValue,0x48);
                Delay_ms(500);
              }
            break;
          case 3:   // Days
            hexValue = DS1307readByte(DS1307_ADDRESS, (unsigned int) 4);
            decimalValue = toDecimal(hexValue);
            showDecimal(decimalValue,0x48);
            if (checkButtons(&decimalValue) )
              {
                DS1307writeByte(DS1307_ADDRESS,
                        (unsigned int)4,
                        toHex(decimalValue));
                Delay_ms(10); // AT24C16 has 5 ms write cycle time
                showDecimal(decimalValue,0x48);
                Delay_ms(500);
                adjustDayOfWeek();
              }
            break;
          case 4:   // Months
            hexValue = DS1307readByte(DS1307_ADDRESS, (unsigned int) 5);
            decimalValue = toDecimal(hexValue);
            showDecimal(decimalValue,0x48);
            if (checkButtons(&decimalValue) )
              {
                DS1307writeByte(DS1307_ADDRESS,
                        (unsigned int)5,
                        toHex(decimalValue));
                Delay_ms(10); // AT24C16 has 5 ms write cycle time
                showDecimal(decimalValue,0x48);
                Delay_ms(500);
              }
            break;
          case 5:   // Years
            hexValue = DS1307readByte(DS1307_ADDRESS, (unsigned int) 6);
            decimalValue = toDecimal(hexValue)+2000;
            showDecimal(decimalValue,0x48);
            if (checkButtons(&decimalValue) )
              {
                DS1307writeByte(DS1307_ADDRESS,
                        (unsigned int)6,
                        toHex(18));
                Delay_ms(10); // AT24C16 has 5 ms write cycle time
                showDecimal(decimalValue,0x48);
                Delay_ms(500);
              }
            break;
        }
    }

  return 0;
}
